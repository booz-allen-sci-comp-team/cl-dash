from distutils.core import setup
setup(name='cl-dash',
      version='1.0.0',
      description='cl-dash Hadoop Cluster Management on AWS',
      packages=['cl_dash'],
      scripts=['cl-create', 'cl-display', 'cl-list', 'cl-start', 'cl-stop',
               'cl-terminate', 'menu_module'],
      package_data = {'cl_dash': ['example.yml']}
)
