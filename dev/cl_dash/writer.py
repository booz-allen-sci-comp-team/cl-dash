import sys
import os
import curses
import time

class WriterStd(object):

    def __init__(self,con):
        self.con = con

    def write(self, str):
        self.con.write(str)

    def writefl(self,str):
        self.write(str)
        self.write("\n")
        self.flush()

    def flush(self):
        self.con.flush()

    def clear(self):
        os.system('cls' if os.name == 'nt' else 'clear')
        #True

class WriterCurses(object):
    len = 57
    r = 21
    c = 2

    def __init__(self,con):
        self.con = con

    def write(self, str):
        str = str[0:WriterCurses.len]
        str = str.ljust(WriterCurses.len)
        self.clear()
        self.con.addstr(WriterCurses.r,WriterCurses.c,str)

    def writefl(self,str):
        self.write(str)
        self.flush()

    def flush(self):
        self.con.refresh()

    def clear(self):
        self.con.addstr(WriterCurses.r,WriterCurses.c,"".ljust(WriterCurses.len))
        self.con.addstr(WriterCurses.r-1,WriterCurses.c,"".ljust(WriterCurses.len))
    
    def input(self, str):
        str = str[0:WriterCurses.len]
        str = str.ljust(WriterCurses.len)
        self.clear()
        self.con.addstr(WriterCurses.r-1,WriterCurses.c,str)
        self.flush()
        self.con.move(WriterCurses.r,WriterCurses.c)
        curses.echo() # Enable echoing of characters
        tmp = self.con.getstr()
        curses.noecho()
        return tmp
        
        


#import os
#os.system('cls' if os.name == 'nt' else 'clear')

#<type '_curses.curses window'>
