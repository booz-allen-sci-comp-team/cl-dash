import curses
from curses import panel
import boto
import boto.manage.cmdshell
import time
import os
import paramiko
import sys

from cluster import Cluster
from menu import Menu
from writer import WriterCurses

#####################################################################

class MyApp(object):
    def __init__(self,stdscreen):
        self.screen = stdscreen
        curses.curs_set(0)
        self.c = Cluster(WriterCurses(self.screen))
        
        self.title_win = self.screen.subwin(3,20,0,0) # height,width,y_begin,x_begin
        #self.title_win.border(' ', ' ', ' ', '-', ' ', ' ', ' ', ' ')
        curses.init_pair(1, curses.COLOR_RED, curses.COLOR_BLACK)
        curses.init_pair(2, curses.COLOR_CYAN, curses.COLOR_BLACK)
        self.title_win.addstr(1,1, "Cluster Setup Tool", curses.color_pair(1))

        config_menu_items = [
                ('Edit Cluster Name', self.set_clustername),
                ('Edit AMI', self.set_image),
                ('Edit Security Group', self.set_securitygroup),
                ('Edit Master Instance Type', self.set_masterinstancetype),
                ('Edit Slave Instance Type', self.set_slaveinstancetype),
                ('Edit Number of Slave Nodes', self.set_slavecount),
                ('Edit AWS SSH Key Name', self.set_keyname),
                ('Edit SSH Key Location (Full Path)', self.set_keydir)
                ]
        self.config_menu = Menu(config_menu_items, self.screen,"config")
        
        launch_menu_items = [
                ('Edit Configuration Parameters', self.config_menu_helper),
                ('Dry Run', self.c.launch_instance_dryrun),
                ('Actual Run', self.c.launch_instances)
                ]
        self.launch_menu = Menu(launch_menu_items, self.screen,"launch")
        
        main_menu_items = [
                ('View Launch Menu', self.launch_menu_helper),
                ('Stop Existing Cluster', self.stop_clusters),
                ('Start Existing (Stopped) Cluster', curses.flash),
                ('Terminate Existing Cluster', self.terminate_clusters),
                ('List Existing Clusters', self.list_clusters)
                ]
        self.main_menu = Menu(main_menu_items, self.screen,"main")#self.screen)
        self.main_menu.display()

    def launch_menu_helper(self):
        array = self.c.get_config()
        for index, line in enumerate(array):
            self.launch_menu.window2.addstr(1+index, 1, line[0], curses.A_NORMAL)
            self.launch_menu.window2.addstr(1+index, 25, str(line[1]), curses.color_pair(2))
        #self.launch_menu.window2.border('|', '|', '-', '-', '+', '+', '+', '+')
        #self.launch_menu.window2.refresh()
        panel.update_panels()
        curses.doupdate()
        self.launch_menu.display()
    
    def config_menu_helper(self):
        self.update_config_display()
        self.config_menu.display()
    
    def update_config_display(self):
        array = self.c.get_config()
        self.config_menu.window2.clear()
        for index, line in enumerate(array):
            self.config_menu.window2.addstr(1+index, 1, line[0], curses.A_NORMAL)
            self.config_menu.window2.addstr(1+index, 25, str(line[1]), curses.color_pair(2))
        #self.config_menu.window2.border('|', '|', '-', '-', '+', '+', '+', '+')
        
        array = self.c.get_config()
        self.launch_menu.window2.clear()
        for index, line in enumerate(array):
            self.launch_menu.window2.addstr(1+index, 1, line[0], curses.A_NORMAL)
            self.launch_menu.window2.addstr(1+index, 25, str(line[1]), curses.color_pair(2))
        #self.launch_menu.window2.border('|', '|', '-', '-', '+', '+', '+', '+')
        
        
        #self.launch_menu.window2.refresh()
        panel.update_panels()
        curses.doupdate()
    
    def discover_clusters(self):
        all_res = self.c.conn.get_all_reservations()
        clusters = []
        for R in all_res:
            for instance in R.instances:
                if 'Type' in instance.tags and instance.tags['Type'] == 'master':
                        clusters.append((instance.tags['Name'],instance.state,instance))
        return clusters
    
    def list_clusters(self):
        clusters = self.discover_clusters()
        self.main_menu.window2.clear()
        for index, line in enumerate(clusters):
            self.main_menu.window2.addstr(1+index, 1, '%d. %s' % (index+1,line[0]), curses.A_NORMAL)
            self.main_menu.window2.addstr(1+index, 25, line[1], curses.color_pair(2))
        #self.main_menu.window2.border('|', '|', '-', '-', '+', '+', '+', '+')
        panel.update_panels()
        curses.doupdate()
    
    def terminate_clusters(self):
        clusters = self.discover_clusters()
        self.main_menu.window2.clear()
        self.main_menu.window2.addstr(1,1,'Please make a selection:')
        for index, line in enumerate(clusters):
            self.main_menu.window2.addstr(3+index, 1, '%d. %s' % (index+1,line[0]), curses.A_NORMAL)
            self.main_menu.window2.addstr(3+index, 25, line[1], curses.color_pair(2))
        #self.main_menu.window2.border('|', '|', '-', '-', '+', '+', '+', '+')
        panel.update_panels()
        curses.doupdate()
        key = self.main_menu.window.getch()
        instances_list = [str(clusters[key-49][2].id)]
        slave_res = clusters[key-49][2].tags['SlaveRes'].encode('ascii','ignore')
        for R in self.c.conn.get_all_reservations():
            if str(R) == slave_res:
                for inst in R.instances:
                    instances_list.append(str(inst.id))
        self.c.conn.terminate_instances(instance_ids=instances_list)
        self.list_clusters()
        
        
    def stop_clusters(self):
        clusters = self.discover_clusters()
        self.main_menu.window2.clear()
        self.main_menu.window2.addstr(1,1,'Please make a selection:')
        for index, line in enumerate(clusters):
            self.main_menu.window2.addstr(3+index, 1, '%d. %s' % (index+1,line[0]), curses.A_NORMAL)
            self.main_menu.window2.addstr(3+index, 25, line[1], curses.color_pair(2))
        #self.main_menu.window2.border('|', '|', '-', '-', '+', '+', '+', '+')
        panel.update_panels()
        curses.doupdate()
        key = self.main_menu.window.getch()
        instances_list = [str(clusters[key-49][2].id)]
        slave_res = clusters[key-49][2].tags['SlaveRes'].encode('ascii','ignore')
        for R in self.c.conn.get_all_reservations():
            if str(R) == slave_res:
                for inst in R.instances:
                    instances_list.append(str(inst.id))
        self.c.conn.stop_instances(instance_ids=instances_list)
        self.list_clusters()
        
    def set_clustername(self):
        self.c.CLUSTER_NAME = self.c.scr.input('Enter Cluster Name')
        self.c.scr.clear()     
        self.c.scr.flush()
        self.update_config_display()
        
    def set_image(self):
        self.c.IMAGE = self.c.scr.input('Enter AMI ID')
        self.c.scr.clear()     
        self.c.scr.flush()
        self.update_config_display()
                 
    def set_securitygroup(self):
        self.c.SECURITY_GROUPS = self.c.scr.input('Enter Security Groups (comma separated)')   
        self.c.scr.clear()     
        self.c.scr.flush()
        self.update_config_display()
          
    def set_masterinstancetype(self):
        self.c.MASTER_INSTANCE_TYPE = self.c.scr.input('Enter Master Instance Type')
        self.c.scr.clear()     
        self.c.scr.flush()
        self.update_config_display()
             
    def set_slaveinstancetype(self):
        self.c.SLAVE_INSTANCE_TYPE = self.c.scr.input('Enter Slave Instance Type')    
        self.c.scr.clear()     
        self.c.scr.flush()
        self.update_config_display()
             
    def set_slavecount(self):
        self.c.SLAVE_COUNT = self.c.scr.input('Enter Number of Slave Nodes')     
        self.c.scr.clear()     
        self.c.scr.flush()
        self.update_config_display()

    def set_keyname(self):
        self.c.KEY_NAME = self.c.scr.input('Enter Key Name as Appears on AWS')
        self.c.scr.clear()     
        self.c.scr.flush()
        self.update_config_display()
        
    def set_keydir(self):
        self.c.KEY_FILE = self.c.scr.input('Enter Path to Key File (e.g. /home/ubuntu/BigBio8-28.pem')
        self.c.scr.clear()     
        self.c.scr.flush()
        self.update_config_display()



