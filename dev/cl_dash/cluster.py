import boto
import boto.manage.cmdshell
import time
import os
import paramiko
import sys
import yaml
import re
import socket

class Cluster(object):
    
    def __init__(self,stdscreen):
        self.scr = stdscreen

        # Variables that have to be provided by the user
        self.KEY_FILE             = '/home/ubuntu/Big Bio 8-28.pem'

        # Variables provided by the user, or read from AWS
        self.IMAGE                = 'ami-5bf7ca32' # Hadoop/zookeeper/hbase
        self.AVAIL_ZONE           = 'us-east-1c' # Default availability zone
        self.KEY_NAME             = 'Big Bio 8-28' # should match AWS name
        self.SECURITY_GROUPS      = ['hadoop master'] # If multiple, separate by commas
        self.CLUSTER_NAME         = 'Aman Test Cluster'
        self.PROJECT_NAME         = 'Testing project'
        self.PROJECT_LEAD         = 'Paul Hodor'
        self.CHARGE_NR            = 'N00000000000000'
        self.SLAVE_INSTANCE_TYPE  = 't1.micro'
        self.SLAVE_COUNT          = 1 # number of slaves to start, 1 master will be started in addition
        self.MASTER_INSTANCE_TYPE = 't1.micro'

        # Derived variables
        self.public_dns           = [] # public dns 
        self.instanceinfo         = {} # key is value of AWS tag 'Type', value is private IP address of instance
        self.inst_m               = [] # single value of master boto instance, keep as array for consistency
        self.inst_s               = [] # array of slaves as boto instances

        self.connect()

    def connect(self):
        self.conn = boto.connect_ec2()
        self.OWNER = boto.connect_iam().get_user().user_name.encode('ascii','ignore')

    def clear(self):
        self.KEY_FILE             = None

        self.IMAGE                = None
        self.AVAIL_ZONE           = None
        self.KEY_NAME             = None
        self.SECURITY_GROUPS      = []
        self.CLUSTER_NAME         = None
        self.PROJECT_NAME         = None
        self.PROJECT_LEAD         = None
        self.CHARGE_NR            = None
        self.SLAVE_INSTANCE_TYPE  = None
        self.SLAVE_COUNT          = None
        self.MASTER_INSTANCE_TYPE = None

        self.public_dns           = []
        self.instanceinfo         = {}

    def __repr__(self):
        res = "Cluster:\n"
        res = res + '  KEY_FILE: %s'             % self.KEY_FILE             + '\n'
        res = res + '  IMAGE: %s'                % self.IMAGE                + '\n'
        res = res + '  AVAIL_ZONE: %s'           % self.AVAIL_ZONE           + '\n'
        res = res + '  KEY_NAME: %s'             % self.KEY_NAME             + '\n'
        res = res + '  SECURITY_GROUPS: %s'      % self.SECURITY_GROUPS      + '\n'
        res = res + '  CLUSTER_NAME: %s'         % self.CLUSTER_NAME         + '\n'
        res = res + '  PROJECT_NAME: %s'         % self.PROJECT_NAME         + '\n'
        res = res + '  PROJECT_LEAD: %s'         % self.PROJECT_LEAD         + '\n'
        res = res + '  CHARGE_NR: %s'            % self.CHARGE_NR            + '\n'
        res = res + '  SLAVE_INSTANCE_TYPE: %s'  % self.SLAVE_INSTANCE_TYPE  + '\n'
        res = res + '  SLAVE_COUNT: %s'          % self.SLAVE_COUNT          + '\n'
        res = res + '  MASTER_INSTANCE_TYPE: %s' % self.MASTER_INSTANCE_TYPE + '\n'
        res = res + '  OWNER: %s'                % self.OWNER                + '\n'
        res = res + '  master instances: %s'     % [str(inst.id) for inst in self.inst_m] + '\n'
        res = res + '  slave instances: %s'      % [str(inst.id) for inst in self.inst_s] + '\n'
        res = res + '  public_dns: %s'           % self.public_dns           + '\n'
        res = res + '  instanceinfo: %s'         % self.instanceinfo         + '\n'
        return res

    def check_image(self):
        # Check to see if AMI ID that is provided actually exists
        flag = True
        try:
            img = self.conn.get_all_images(image_ids=[self.IMAGE])[0]
        except self.conn.ResponseError, e:
            if e.code == 'InvalidAMIID.Malformed':
                flag = False
            else:
                raise
        self.scr.writefl('AMI "%s" exists on AWS: %s' % (self.IMAGE, flag))
        return flag #false if AMI doesn't exist

    def check_zone(self):
        # Check that the availability zone is valid
        flag = self.AVAIL_ZONE in [z.name for z in self.conn.get_all_zones()]
        self.scr.writefl('Valid availability zone "%s": %s' % (self.AVAIL_ZONE, flag))
        return flag

    def check_keyfile(self):
        # Check that the pem key file exists on the local file system
        flag = True
        if not os.path.isfile(self.KEY_FILE):
            flag = False
        self.scr.writefl('Key file "%s" exists on local file system: %s' % (self.KEY_FILE, flag))
        return flag # if false, the key file doesn't exist

    def check_keypair(self):
        # Check to see if specified keypair already exists on AWS.
        # If we get an InvalidKeyPair.NotFound error back from EC2,
        # it means that it doesn't exist and we return an error flag.
        flag = True
        try:
            key = self.conn.get_all_key_pairs(keynames=[self.KEY_NAME])[0]
        except self.conn.ResponseError, e:
            if e.code == 'InvalidKeyPair.NotFound':
                flag = False
            else:
                raise
        self.scr.writefl('Key pair "%s" exists on AWS: %s' % (self.KEY_NAME, flag))
        return flag # if false, the key pair doesn't exist on AWS

    def check_securitygroup(self):
        # Check to see if all specified security groups already exists.
        # If we get an InvalidGroup.NotFound error back from EC2 for any group,
        # it means that it doesn't exist and we return an error flag.
        flag = True
        for group_name in self.SECURITY_GROUPS:
            try:
                group = self.conn.get_all_security_groups(groupnames=[group_name])[0]
            except self.conn.ResponseError, e:
                if e.code == 'InvalidGroup.NotFound':
                    self.scr.writefl('Security group "%s" does not exist' % group_name)
                    flag = False
                else:
                    raise
        self.scr.writefl('Security groups all valid: %s' % flag)
        return flag # if false, at least one of the security groups does not exist
        
    def run_checks(self):
        flag = True
        flag = self.check_image() and flag
        flag = self.check_zone() and flag
        flag = self.check_keyfile() and flag
        flag = self.check_keypair() and flag
        flag = self.check_securitygroup() and flag
        return flag # returns true if all checks are true

    def launch_cmd(self,dry_run=False):
        """
        Send commands to AWS to launch cluster instances

        :dry_run: True if this is a dry run, False if it is an actual run
        """
        SLAVE_RESERVATION = self.conn.run_instances(
            self.IMAGE, placement=self.AVAIL_ZONE, instance_type=self.SLAVE_INSTANCE_TYPE,
            key_name=self.KEY_NAME, security_groups=self.SECURITY_GROUPS,
            min_count=self.SLAVE_COUNT, max_count=self.SLAVE_COUNT, dry_run=dry_run)
        MASTER_RESERVATION = self.conn.run_instances(
            self.IMAGE, placement=self.AVAIL_ZONE, instance_type=self.MASTER_INSTANCE_TYPE,
            key_name=self.KEY_NAME, security_groups=self.SECURITY_GROUPS,
            min_count=1, max_count=1, dry_run=dry_run)

        # save instances into Cluster object
        self.inst_m = MASTER_RESERVATION.instances
        self.inst_s = SLAVE_RESERVATION.instances


    def poll_running_all(self):
        """
        Wait for a list of AWS instances until all have a status different
        from "pending". If not all instances change status to "running", emit
        an error message and exit.
        """

        instances = self.inst_m + self.inst_s
        nrinst = len(instances)

        # initialize dictionary of status count for all instances
        status = {"pending": nrinst}

        self.scr.writefl("Waiting for %d instances to start:" % nrinst)
        while "pending" in status: # wait until there are no "pending" instances
            time.sleep(5) # Wait for instances to start up
            status = dict() # reinitialize the status count
            for instance in instances:
                crt_status = instance.update()
                if crt_status in status:
                    status[crt_status] += 1
                else:
                    status[crt_status] = 1
            for crt_status in sorted(status):
                msg = "  %s: %d" % (crt_status, status[crt_status])
                self.scr.writefl(msg)
            self.scr.writefl("  .")
        if (not "running" in status) or (status["running"] != nrinst):
            self.scr.writefl("Exiting because not all instances are running.")
            sys.exit()
        else:
            self.scr.writefl("  ok")

    def tag_instances(self):
        """
        Tag a list of AWS instances
        """
        # tag all instances with common tags
        for i, instance in enumerate(self.inst_m + self.inst_s):
            msg = "Tagging instance %s" % str(instance.id)
            self.scr.writefl(msg)

            maxtrials = 5
            trial = 0
            while(trial < maxtrials):
                trial += 1
                try:
                    instance.add_tag("Name",self.CLUSTER_NAME)
                    instance.add_tag("Project",self.PROJECT_NAME)
                    instance.add_tag("Project Lead",self.PROJECT_LEAD)
                    instance.add_tag("Charge Number",self.CHARGE_NR)
                    instance.add_tag("Owner",self.OWNER)
                except:
                    if trial >= maxtrials:
                        raise
                    else:
                        time.sleep(5)
            self.scr.writefl("  ok")

        # tag master with Type
        self.inst_m[0].add_tag('Type','master')
        # tags the slaves with node numbers appended (e.g. slave1, slave2)
        for i, instance in enumerate(self.inst_s, start=1):
            instance.add_tag('Type','slave%s' % i)


    def poll_ssh_all(self):
        """
        Wait for a list of AWS instances to be all reachable by ssh
        """
        for instance in self.inst_m + self.inst_s:
            self.poll_ssh(instance)

    def poll_ssh(self, instance):
        """
        Wait for an AWS instance to be reachable by ssh

        :instance: boto instance to wait for
        """
        msg = 'Polling SSH on %s ' % str(instance.id)
        self.scr.writefl(msg)
        retry = True
        while retry:
            try:
                ssh = paramiko.SSHClient()
                ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy()) # wont require saying 'yes' to new fingerprint
                ssh.connect(instance.public_dns_name,username='ubuntu',key_filename=self.KEY_FILE)
                ssh.close()
                retry = False
                self.scr.writefl('Successfully connected via SSH to %s' % instance)
            # errors 113 'No route to host' and 111 'Connection refused' are part of the normal startup process
            except socket.error as e:
                if (e.args[0] == 113):
                    msg = msg + "."
                    self.scr.writefl(msg)
                    time.sleep(5)
                elif (e.args[0] == 111):
                    msg = msg + "+"
                    self.scr.writefl(msg)
                    time.sleep(5)
                else:
                    self.scr.writefl("Unexpected socket error: %s" % str(e))
                    raise
            except Exception as e:
                self.scr.writefl("Other error: %s" % str(e))
                raise

    def save_instance_data(self):
        """
        Save additional instance info in object's private data structures
        """
        for instance in self.inst_m + self.inst_s:
            self.instanceinfo[str(instance.tags['Type'])] = str(instance.private_ip_address)
            self.public_dns.append(str(instance.public_dns_name))

    def update_hosts_file(self, try_times=3):
        """
        Update /etc/hosts file in all cluster instances

        :try_times: Number of times to retry in case of failure
        """
        # create contents for /etc/hosts
        hosts = ['127.0.0.1 localhost\n']
        for tag,ip in self.instanceinfo.iteritems():
            hosts.append('%s %s\n' % (ip,tag))

        # iterate over instances
        for dns in self.public_dns:
            msg = "Writing /etc/hosts to %s" % str(dns)
            self.scr.writefl(msg)

            # Try multiple times
            success = False
            try_i = 0
            while not success and try_i < try_times:
                ssh = paramiko.SSHClient()
                # wont require saying 'yes' to new fingerprint
                ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
                #print(dns)
                #print(self.KEY_FILE)
                ssh.connect(dns,username='ubuntu',key_filename=self.KEY_FILE)

                # write file to server
                sftp = paramiko.SFTPClient.from_transport(ssh.get_transport())
                f = sftp.open('/home/ubuntu/hosts', 'w')
                f.writelines(hosts)
                f.close()
                sftp.close()

                # move file to correct location and set ownership
                streams = ssh.exec_command('sudo mv /home/ubuntu/hosts /etc/hosts'
                                 + ' && sudo chown root:root /etc/hosts'
                                 + ' && sudo chmod 644 /etc/hosts')
                status = streams[1].channel.recv_exit_status()

                # read back the remote file
                hosts_remote = ''
                try:
                    sftp = paramiko.SFTPClient.from_transport(ssh.get_transport())
                    f = sftp.open('/etc/hosts', 'r')
                    hosts_remote = f.readlines()
                    f.close()
                    sftp.close()
                except Exception:
                    pass
                
                # check that the remote file was saved correctly
                if hosts == hosts_remote:
                    success = True
                    self.scr.writefl("  ok")
                else:
                    self.scr.writefl("  retrying...")

                # close ssh connection
                ssh.close()

                try_i = try_i + 1

            if not success:
                raise Exception("Could not update /etc/hosts file")


    def ssh_all_nodes(self, try_times=3):
        """
        Log in from each instance to each address in the cluster

        :try_times: Number of times to retry in case of failure
        """
        self.scr.writefl('Performing ssh connections')

        # make list of commands
        cmd = ['sudo su hduser -c "ssh -o StrictHostKeyChecking=no 127.0.0.1 hostname"',
              'sudo su hduser -c "ssh -o StrictHostKeyChecking=no localhost hostname"',
              'sudo su hduser -c "ssh -o StrictHostKeyChecking=no 0.0.0.0 hostname"']
        for tag,ip in self.instanceinfo.iteritems():
            cmd.append('sudo su hduser -c "ssh -o StrictHostKeyChecking=no %s hostname"' % tag)
        
        self.scr.writefl("  Commands:")
        for c in cmd:
            self.scr.writefl("      %s" % c)

        # iterate over instances
        for dns in self.public_dns:
            self.scr.writefl("  Working from %s" % dns)

            # Try multiple times
            success = False
            try_i = 0
            while not success and try_i < try_times:
                ssh = paramiko.SSHClient()
                # wont require saying 'yes' to new fingerprint
                ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
                ssh.connect(dns,username='ubuntu',key_filename=self.KEY_FILE)

                # remove old known_hosts file
                streams = ssh.exec_command('sudo rm /home/hduser/.ssh/known_hosts')
                status = streams[1].channel.recv_exit_status()

                success = True
                for c in cmd:
                    host_name = ssh.exec_command(c)[1].readline().rstrip()
                    if host_name != '':
                        self.scr.writefl("      :%s: connected" % host_name)
                    else:
                        success = False
                        self.scr.writefl("      empty host name")
                ssh.close()

                try_i = try_i + 1

            if not success:
                raise Exception("ssh login completed with errors")

    def update_bashrc_file(self, try_times=3):
        """
        Update /home/hduser/.bashrc file on all cluster instances

        :try_times: Number of times to retry in case of failure
        """
        # create dynamic content to append to skeleton file
        master_dns = str(self.public_dns[0])
        append_arr = ['\n']
        append_arr.append('export HTTPFS_HTTP_HOSTNAME=%s\n' % master_dns)
        append_arr.append('export OOZIE_HTTP_HOSTNAME=%s\n' % master_dns)
        append_arr.append('export OOZIE_URL=http://%s:11000/oozie\n' % master_dns)

        #print append_arr
        
        # iterate over instances
        for dns in self.public_dns:
            msg = "Writing /home/hduser/.bashrc to %s" % str(dns)
            self.scr.writefl(msg)
        
            # Try multiple times
            success = False
            try_i = 0
            while not success and try_i < try_times:
                ssh = paramiko.SSHClient()
                # wont require saying 'yes' to new fingerprint
                ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
                #print(dns)
                #print(self.KEY_FILE)
                ssh.connect(dns,username='ubuntu',key_filename=self.KEY_FILE)

                # write array with new environment variables to tmp file on server
                sftp = paramiko.SFTPClient.from_transport(ssh.get_transport())
                f = sftp.open('/home/ubuntu/tmp', 'w')
                f.writelines(append_arr)
                f.close()
                sftp.close()
        
                # move tmp file to hduser dir, set ownership, and create .bashrc file
                streams = ssh.exec_command('sudo mv /home/ubuntu/tmp /home/hduser/.bashrc.tmp'
                                           + ' && sudo chown hduser:hadoop /home/hduser/.bashrc.tmp'
                                           + ' && sudo chmod 644 /home/hduser/.bashrc.tmp'
                                           + ' && sudo sh -c \'cat /home/hduser/.bashrc.skel '
                                           + '/home/hduser/.bashrc.tmp > /home/hduser/.bashrc\''
                                           + ' && sudo chown hduser:hadoop /home/hduser/.bashrc'
                                           + ' && sudo chmod 644 /home/hduser/.bashrc')
                status = streams[1].channel.recv_exit_status()

                # read back the remote files
                bashrc = []
                bashrc_skel = []
                try:
                    sftp = paramiko.SFTPClient.from_transport(ssh.get_transport())
                    f = sftp.open('/home/hduser/.bashrc', 'r')
                    bashrc = f.readlines()
                    f.close()
                    f = sftp.open('/home/hduser/.bashrc.skel', 'r')
                    bashrc_skel = f.readlines()
                    f.close()
                    sftp.close()
                except Exception:
                    pass
                
                # close ssh connection
                ssh.close()
        
                # check that the remote .bashrc file was saved correctly
                if bashrc == (bashrc_skel + append_arr):
                    success = True
                    self.scr.writefl("  ok")
                else:
                    self.scr.writefl("  retrying...")
                
                try_i = try_i + 1
        
            if not success:
                raise Exception("Could not update /etc/hosts file")


    def start_zookeeper(self, try_times=6):
        """
        Start zookeeper on all nodes
        """

        # start zookeeper on all nodes
        self.scr.writefl("Starting zookeeper on all nodes")
        for instance in self.inst_m + self.inst_s:
            ssh = paramiko.SSHClient()
            # wont require saying 'yes' to new fingerprint
            ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            ssh.connect(instance.public_dns_name,username='ubuntu',key_filename=self.KEY_FILE)
            streams = ssh.exec_command('sudo su hduser -c "/usr/local/zookeeper/bin/zkServer.sh start"')
            status = streams[1].channel.recv_exit_status()
            ssh.close()
        
        # check that zookeeper is running on all nodes
        success = False
        try_i = 0
        while not success and try_i < try_times:
            success = True # failure on any node will change to False
            for instance in self.inst_m + self.inst_s:
                ssh = paramiko.SSHClient() 
                ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
                ssh.connect(instance.public_dns_name,username='ubuntu',key_filename=self.KEY_FILE)
                streams = ssh.exec_command('sudo su hduser -c "/usr/local/zookeeper/bin/zkServer.sh status"')
                mylines = [line.rstrip() for line in streams[1].readlines()]
                myelines = [line.rstrip() for line in streams[2].readlines()]
                status = streams[1].channel.recv_exit_status()
                ssh.close()
                
                # check for error
                for line in mylines:
                    self.scr.writefl("    instance %s: %s" % (instance.id, line))
                    if re.match(r'^Error', line):
                        success = False
                if status != 0:
                    self.scr.writefl("    instance %s: %s" % (instance.id, myelines))
                    success = False

            if not success:
                self.scr.writefl("  retrying...")
                time.sleep(2)

            try_i = try_i + 1

        if not success:
            raise Exception("Could not start zookeeper")


    def format_namenode(self):
        """
        Format the hdfs namenode
        """

        ### TO DO: How can we check that the namenode has already been formatted?

        self.scr.writefl("Formatting name node")
        ssh = paramiko.SSHClient()
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy()) # wont require saying 'yes' to new fingerprint
        ssh.connect(self.inst_m[0].public_dns_name,username='ubuntu',key_filename=self.KEY_FILE)
        streams = ssh.exec_command('sudo su hduser -c "/usr/local/hadoop/bin/hdfs namenode -format -force"')
        status = streams[1].channel.recv_exit_status()
        if status == 0:
            self.scr.writefl("  ok")
        else:
            self.scr.writefl([line.rstrip() for line in streams[2].readlines()])
            raise Exception("Could not format namenode")

    def setup_oozie(self):
        """
        Create oozie shared library path, initialize database"
        """
        self.scr.writefl("Initializing oozie sharelib and db")
        ssh = paramiko.SSHClient()
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy()) # wont require saying 'yes' to new fingerprint
        ssh.connect(self.inst_m[0].public_dns_name,username='ubuntu',key_filename=self.KEY_FILE)
        streams = ssh.exec_command('sudo su hduser -c "/usr/local/oozie/bin/oozie-setup.sh db create -run" '
        + '&& sudo su hduser -c "/usr/local/oozie/bin/oozie-setup.sh sharelib create -fs hdfs://master:9000"')
        status = streams[1].channel.recv_exit_status()
        if status == 0:
            self.scr.writefl("  ok")
        else:
            self.scr.writefl([line.rstrip() for line in streams[2].readlines()])
            raise Exception("Could not initialize oozie")

        ### TO DO: Need to check that namenode and data nodes are working
    
    def start_dfs(self):
        """
        Start the file system
        """
        self.scr.writefl("Starting the file system")
        ssh = paramiko.SSHClient()
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy()) # wont require saying 'yes' to new fingerprint
        ssh.connect(self.inst_m[0].public_dns_name,username='ubuntu',key_filename=self.KEY_FILE)
        streams = ssh.exec_command('sudo su hduser -c "/usr/local/hadoop/sbin/start-dfs.sh"')
        status = streams[1].channel.recv_exit_status()
        if status == 0:
            self.scr.writefl("  ok")
        else:
            self.scr.writefl([line.rstrip() for line in streams[2].readlines()])
            raise Exception("Could not start file system")

        ### TO DO: Need to check that namenode and data nodes are working
       

    def start_yarn(self):
        """
        Start the yarn service
        """
        self.scr.writefl("Starting the yarn service")
        ssh = paramiko.SSHClient()
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy()) # wont require saying 'yes' to new fingerprint
        ssh.connect(self.inst_m[0].public_dns_name,username='ubuntu',key_filename=self.KEY_FILE)
        streams = ssh.exec_command('sudo su hduser -c "/usr/local/hadoop/sbin/start-yarn.sh"')
        status = streams[1].channel.recv_exit_status()
        if status == 0:
            self.scr.writefl("  ok")
        else:
            self.scr.writefl([line.rstrip() for line in streams[2].readlines()])
            raise Exception("Could not start yarn service")

        ### TO DO: Need to check that yarn is working

    def start_historyserver(self):
        """
        Start MapReduce Job History Server
        """
        self.scr.writefl("Starting Map Reduce Job History server")
        ssh = paramiko.SSHClient()
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy()) # wont require saying 'yes' to new fingerprint
        ssh.connect(self.inst_m[0].public_dns_name,username='ubuntu',key_filename=self.KEY_FILE)
        streams = ssh.exec_command('sudo su hduser -c "/usr/local/hadoop/sbin/mr-jobhistory-daemon.sh start historyserver --config $HADOOP_CONF_DIR"')
        status = streams[1].channel.recv_exit_status()
        if status == 0:
            self.scr.writefl("  ok")
        else:
            # note: job history server doesn't seem to write anything to stderr?
            self.scr.writefl([line.rstrip() for line in streams[2].readlines()])
            raise Exception("Could not start MR Job History Server")

    def start_httpfs(self):
        """
        Start HTTPFS
        """
        self.scr.writefl("Starting HTTPFS")
        ssh = paramiko.SSHClient()
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy()) # wont require saying 'yes' to new fingerprint
        ssh.connect(self.inst_m[0].public_dns_name,username='ubuntu',key_filename=self.KEY_FILE)
        streams = ssh.exec_command('sudo su hduser -c "/usr/local/hadoop/sbin/httpfs.sh start"')
        status = streams[1].channel.recv_exit_status()
        if status == 0:
            self.scr.writefl("  ok")
        else:
            # note: job history server doesn't seem to write anything to stderr?
            self.scr.writefl([line.rstrip() for line in streams[2].readlines()])
            raise Exception("Could not start HTTPFS")

    def start_oozie(self):
        """
        Start Oozie
        """
        self.scr.writefl("Starting oozie")
        ssh = paramiko.SSHClient()
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy()) # wont require saying 'yes' to new fingerprint
        ssh.connect(self.inst_m[0].public_dns_name,username='ubuntu',key_filename=self.KEY_FILE)
        streams = ssh.exec_command('sudo su hduser -c "/usr/local/oozie/bin/oozied.sh start"')
        status = streams[1].channel.recv_exit_status()
        if status == 0:
            self.scr.writefl("  ok")
        else:
            # note: job history server doesn't seem to write anything to stderr?
            self.scr.writefl([line.rstrip() for line in streams[2].readlines()])
            raise Exception("Could not start oozie")


    def start_hbase(self):
        """
        Start HBase service
        """
        self.scr.writefl("Starting HBase")
        ssh = paramiko.SSHClient()
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy()) # wont require saying 'yes' to new fingerprint
        ssh.connect(self.inst_m[0].public_dns_name,username='ubuntu',key_filename=self.KEY_FILE)
        streams = ssh.exec_command('sudo su hduser -c "/usr/local/hbase/bin/start-hbase.sh"')
        status = streams[1].channel.recv_exit_status()
        if status == 0:
            self.scr.writefl("  ok")
        else:
            self.scr.writefl([line.rstrip() for line in streams[2].readlines()])
            raise Exception("Could not start HBase")

        ### TO DO: Need to check that HBase is working
      

    def start_hive(self):
        """
        Set up hdfs directories for Hive
        """
        self.scr.writefl("Setting up HDFS directories for Hive")
        ssh = paramiko.SSHClient()
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy()) # wont require saying 'yes' to new fingerprint
        ssh.connect(self.inst_m[0].public_dns_name,username='ubuntu',key_filename=self.KEY_FILE)
        streams = ssh.exec_command('sudo su hduser -c "/usr/local/hadoop/bin/hadoop fs -mkdir /tmp"'
                                   + ' && sudo su hduser -c "/usr/local/hadoop/bin/hadoop fs -mkdir -p /user/hive/warehouse"'
                                   + ' && sudo su hduser -c "/usr/local/hadoop/bin/hadoop fs -chmod g+w /tmp"'
                                   + ' && sudo su hduser -c "/usr/local/hadoop/bin/hadoop fs -chmod g+w /user/hive/warehouse"')
        status = streams[1].channel.recv_exit_status()
        print "hive exit status", status
        if status == 0:
            self.scr.writefl("  ok")
        else:
            #self.scr.writefl([line.rstrip() for line in streams[2].readlines()])
            raise Exception("Could not set up HDFS directories for Hive")

        ### TO DO: Need to check that HBase is working


    def stop_hbase(self):
        """
        Stop HBase service
        """
        self.scr.writefl("Stopping HBase")
        ssh = paramiko.SSHClient()
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        ssh.connect(self.inst_m[0].public_dns_name,username='ubuntu',
                    key_filename=self.KEY_FILE)
        streams = ssh.exec_command('sudo su hduser -c "/usr/local/hbase/bin/stop-hbase.sh"')
        status = streams[1].channel.recv_exit_status()
        if status == 0:
            self.scr.writefl("  ok")
        else:
            self.scr.writefl([line.rstrip() for line in streams[2].readlines()])
            raise Exception("Could not stop HBase")

        ### TO DO: Need to check that HBase is stopped


    def stop_yarn(self):
        """
        Stop the yarn service
        """
        self.scr.writefl("Stopping the yarn service")
        ssh = paramiko.SSHClient()
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        ssh.connect(self.inst_m[0].public_dns_name,username='ubuntu',
                    key_filename=self.KEY_FILE)
        streams = ssh.exec_command('sudo su hduser -c "/usr/local/hadoop/sbin/stop-yarn.sh"')
        status = streams[1].channel.recv_exit_status()
        if status == 0:
            self.scr.writefl("  ok")
        else:
            self.scr.writefl([line.rstrip() for line in streams[2].readlines()])
            raise Exception("Could not stop yarn service")

        ### TO DO: Need to check that yarn is stopped

    def stop_historyserver(self):
        """
        Stop the Map Reduce Job History server
        """
        self.scr.writefl("Stopping the Map Reduce Job History server")
        ssh = paramiko.SSHClient()
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        ssh.connect(self.inst_m[0].public_dns_name,username='ubuntu',
                    key_filename=self.KEY_FILE)
        streams = ssh.exec_command('sudo su hduser -c "/usr/local/hadoop/sbin/mr-jobhistory-daemon.sh stop historyserver --config $HADOOP_CONF_DIR"')
        status = streams[1].channel.recv_exit_status()
        if status == 0:
            self.scr.writefl("  ok")
        else:
            # note: job history server doesn't seem to write anything to stderr?
            #self.scr.writefl([line.rstrip() for line in streams[2].readlines()])
            raise Exception("Could not stop MR Job History server")

    def stop_oozie(self):
        """
        Stop Oozie
        """
        self.scr.writefl("Stopping oozie")
        ssh = paramiko.SSHClient()
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy()) # wont require saying 'yes' to new fingerprint
        ssh.connect(self.inst_m[0].public_dns_name,username='ubuntu',key_filename=self.KEY_FILE)
        streams = ssh.exec_command('sudo su hduser -c "/usr/local/oozie/bin/oozied.sh stop"')
        status = streams[1].channel.recv_exit_status()
        if status == 0:
            self.scr.writefl("  ok")
        else:
            # note: doesn't seem to write anything to stderr?
            self.scr.writefl([line.rstrip() for line in streams[2].readlines()])
            raise Exception("Could not stop oozie")

    def stop_httpfs(self):
        """
        Stop HTTPFS
        """
        self.scr.writefl("Stopping HTTPFS")
        ssh = paramiko.SSHClient()
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy()) # wont require saying 'yes' to new fingerprint
        ssh.connect(self.inst_m[0].public_dns_name,username='ubuntu',key_filename=self.KEY_FILE)
        streams = ssh.exec_command('sudo su hduser -c "/usr/local/hadoop/sbin/httpfs.sh stop"')
        status = streams[1].channel.recv_exit_status()
        if status == 0:
            self.scr.writefl("  ok")
        else:
            # note: doesn't seem to write anything to stderr?
            #self.scr.writefl([line.rstrip() for line in streams[2].readlines()])
            raise Exception("Could not stop HTTPFS")

    def stop_dfs(self):
        """
        Stop the file system
        """
        self.scr.writefl("Stopping the file system")
        ssh = paramiko.SSHClient()
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        ssh.connect(self.inst_m[0].public_dns_name,username='ubuntu',
                    key_filename=self.KEY_FILE)
        streams = ssh.exec_command('sudo su hduser -c "/usr/local/hadoop/sbin/stop-dfs.sh"')
        status = streams[1].channel.recv_exit_status()
        if status == 0:
            self.scr.writefl("  ok")
        else:
            self.scr.writefl([line.rstrip() for line in streams[2].readlines()])
            raise Exception("Could not stop file system")

        ### TO DO: Need to check that namenode and data nodes are stopped


    def stop_zookeeper(self):
        """
        Stop zookeeper on all nodes
        """
        self.scr.writefl("Stopping zookeeper on all nodes")
        for instance in self.inst_m + self.inst_s:
            ssh = paramiko.SSHClient()
            ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            ssh.connect(instance.public_dns_name,username='ubuntu',
                        key_filename=self.KEY_FILE)
            streams = ssh.exec_command('sudo su hduser -c "/usr/local/zookeeper/bin/zkServer.sh stop"')
            status = streams[1].channel.recv_exit_status()
            ssh.close()
        if status == 0:
            self.scr.writefl("  ok")
        else:
            self.scr.writefl([line.rstrip() for line in streams[2].readlines()])
            raise Exception("Could not stop zookeeper")

        ### TO DO: Need to check that zookeeper is stopped


    def edit_config_files(self):
        ### NOTE: The yarn config file /usr/local/hadoop/etc/hadoop/yarn-site.xml
        ### has the name of the master hardcoded as "master".
        for instance in self.inst_m + self.inst_s:
            self.scr.writefl('Editing configuration files on %s' % instance)
            ssh = paramiko.SSHClient()
            ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy()) # wont require saying 'yes' to new fingerprint
            ssh.connect(instance.public_dns_name,username='ubuntu',key_filename=self.KEY_FILE)
            sftp = paramiko.SFTPClient.from_transport(ssh.get_transport())
                
            #/usr/local/hadoop/etc/hadoop/slaves
            slaves=[]
            for tag,ip in self.instanceinfo.iteritems():
                slaves.append(tag)
            ssh.exec_command('touch /home/ubuntu/slaves')
            f = sftp.open('/home/ubuntu/slaves', 'w')
            slaves = '\n'.join(slaves)
            f.writelines(slaves)
            f.close()
            fout = '/usr/local/hadoop/etc/hadoop/slaves'
            streams = ssh.exec_command('sudo mv /home/ubuntu/slaves ' + fout
                                       + ' && sudo chown hduser:hadoop ' + fout
                                       + ' && sudo chmod 644 ' + fout)
            status = streams[1].channel.recv_exit_status()
                
            #/usr/local/hbase/conf/regionservers
            regionservers=[]
            for tag,ip in self.instanceinfo.iteritems():
                regionservers.append(tag)
            ssh.exec_command('touch /home/ubuntu/regionservers')
            f = sftp.open('/home/ubuntu/regionservers', 'w')
            regionservers = '\n'.join(regionservers)
            f.writelines(regionservers)
            f.close()
            fout = '/usr/local/hbase/conf/regionservers'
            streams = ssh.exec_command('sudo mv /home/ubuntu/regionservers ' + fout
                                       + ' && sudo chown hduser:hadoop ' + fout
                                       + ' && sudo chmod 644 ' + fout)
            status = streams[1].channel.recv_exit_status()
                
            #/usr/local/hadoop/etc/hadoop/core-site.xml
            contents=['<?xml version="1.0" encoding="UTF-8"?>',
                      '<?xml-stylesheet type="text/xsl" href="configuration.xsl"?>',
                      '<configuration>',
                      '  <property>',
                      '    <name>fs.default.name</name>',
                      '    <value>hdfs://master:9000</value>',
                      '  </property>',
                      '  <property>',
                      '    <name>hadoop.proxyuser.hduser.hosts</name>',
                      '    <value>*</value>',
                      '  </property>',
                      '  <property>',
                      '    <name>hadoop.proxyuser.hduser.groups</name>',
                      '    <value>hadoop</value>',
                      '  </property>',
                      '  <property>',
                      '    <name>hadoop.tmp.dir</name>',
                      '    <value>/home/hduser/mydata/tmp/hadoop-${user.name}</value>'\
                      ,
                      '  </property>',
                      '</configuration>']
            ssh.exec_command('touch /home/ubuntu/core-site.xml')
            f = sftp.open('/home/ubuntu/core-site.xml', 'w')
            contents = '\n'.join(contents)
            f.writelines(contents)
            f.close()
            fout = '/usr/local/hadoop/etc/hadoop/core-site.xml'
            streams = ssh.exec_command('sudo mv /home/ubuntu/core-site.xml ' + fout
                                       + ' && sudo chown hduser:hadoop ' + fout
                                       + ' && sudo chmod 644 ' + fout)
            status = streams[1].channel.recv_exit_status()
                          
                
            #/usr/local/hbase/conf/hbase-site.xml
            quorum=[]
            contents=['<?xml version="1.0" encoding="UTF-8"?>',
                      '<?xml-stylesheet type="text/xsl" href="configuration.xsl"?>',
                      '<configuration>',
                      '<property>',
                      '<name>hbase.rootdir</name>',
                      '<value>hdfs://master:9000/hbase</value>',
                      '</property>',
                      '<property>',
                      '<name>hbase.cluster.distributed</name>',
                      '<value>true</value>',
                      '</property>',
                      '<property>',
                      '<name>hbase.zookeeper.quorum</name>']
            for tag,ip in self.instanceinfo.iteritems():
                quorum.append(tag)
            q = ','.join(map(str, quorum))
            contents.append('<value>%s</value>' % q)
            c2 = ['</property>',
                  '<property>',
                  '<name>dfs.replication</name>',
                  '<value>1</value>',
                  '</property>',
                  '<property>',
                  '<name>hbase.zookeeper.property.clientPort</name>',
                  '<value>2181</value>',
                  '</property>',
                  '<property>',
                  '<name>hbase.zookeeper.property.dataDir</name>',
                  '<value>/home/zookeeper-data</value>', # is this path right?
                  '</property>',
                  '</configuration>']
            for c in c2:
                contents.append(c)
            ssh.exec_command('touch /home/ubuntu/hbase-site.xml')
            f = sftp.open('/home/ubuntu/hbase-site.xml', 'w')
            contents = '\n'.join(contents)
            f.writelines(contents)
            f.close()
            fout = '/usr/local/hbase/conf/hbase-site.xml'
            streams = ssh.exec_command('sudo mv /home/ubuntu/hbase-site.xml ' + fout
                                       + ' && sudo chown hduser:hadoop ' + fout
                                       + ' && sudo chmod 644 ' + fout)
            status = streams[1].channel.recv_exit_status()
              
            #/usr/local/zookeeper/conf/zoo.cfg
            zoo=['tickTime=2000',
                 'dataDir=/home/hduser/zookeeper-data',
                 'clientPort=2181',
                 'initLimit=5',
                 'syncLimit=2']
            for i,k in enumerate(self.instanceinfo.keys()):
                zoo.append('server.%s=%s:2888:3888' % (i,k))
                if instance.tags['Type']==k:
                    #~/zookeeper-data/myid
                    ssh.exec_command('touch /home/ubuntu/myid')
                    f = sftp.open('/home/ubuntu/myid','w')
                    f.writelines(str(i))
                    f.close()
                    fout = '/home/hduser/zookeeper-data/myid'
                    streams = ssh.exec_command('sudo mv /home/ubuntu/myid ' + fout
                                               + ' && sudo chown hduser:hadoop ' + fout
                                               + ' && sudo chmod 644 ' + fout)
                    status = streams[1].channel.recv_exit_status()
            ssh.exec_command('touch /home/ubuntu/zoo.cfg')
            f = sftp.open('/home/ubuntu/zoo.cfg', 'w')
            zoo = '\n'.join(zoo)
            f.writelines(zoo)
            f.close()
            fout = '/usr/local/zookeeper/conf/zoo.cfg'
            streams = ssh.exec_command('sudo mv /home/ubuntu/zoo.cfg ' + fout
                                       + ' && sudo chown hduser:hadoop ' + fout
                                       + ' && sudo chmod 644 ' + fout)
            status = streams[1].channel.recv_exit_status()

            ssh.close()

        self.scr.writefl('File configuration complete')
                                

    def launch_instance_dryrun(self):
        if self.run_checks():
            try:
                self.launch_cmd(dry_run=True)
            except self.conn.ResponseError, e:
                self.scr.writefl(e.message)
        else:
            self.scr.writefl('Did not pass tests, dry run not attempted')

        
    def launch_instances(self):
        if self.run_checks():
            self.scr.writefl('Launching instances')

            # launch instances
            # lists of instances are set here
            self.launch_cmd()

            # tag instances
            self.tag_instances()

            # wait until all instances are running
            self.poll_running_all()

            # wait until the instance is reachable by ssh
            self.poll_ssh_all()

            # save additional instance data
            self.save_instance_data()

            # write /etc/hosts file
            self.update_hosts_file()

            # ssh from each instance to all others
            self.ssh_all_nodes()

            # write /home/hduser/.bashrc file
            self.update_bashrc_file()

            # edit configuration files on all instances with correct IP addresses
            self.edit_config_files()
            
            # start all Hadoop ecosystem services
            self.start_zookeeper()
            self.format_namenode()
            self.start_dfs()
            self.start_yarn()
            self.start_historyserver()
            self.start_httpfs()
            self.setup_oozie()
            self.start_oozie()
            self.start_hbase()
            #self.start_hive()

            self.scr.writefl('Cluster started')
        else:
            self.scr.writefl('Did not pass tests, not launching cluster')


    def start_cluster(self):
        self.scr.writefl('Starting instances')

        ids = [instance.id for instance in self.inst_m + self.inst_s]
        self.conn.start_instances(instance_ids=ids)

        # wait until all instances are running
        self.poll_running_all()

        # wait until the instance is reachable by ssh
        self.poll_ssh_all()
        
        # re-read cluster information (includes save_instance_data())
        key_file = self.KEY_FILE
        self.get_by_name(self.CLUSTER_NAME)
        self.KEY_FILE = key_file

        # write /etc/hosts file
        self.update_hosts_file()
        
        # ssh from each instance to all others
        self.ssh_all_nodes()

        # write /home/hduser/.bashrc file
        self.update_bashrc_file()

        # edit configuration files on all instances with correct IP addresses
        self.edit_config_files()
            
        # start all Hadoop ecosystem services
        self.start_zookeeper()
        self.start_dfs()
        self.start_yarn()
        self.start_historyserver()
        self.start_httpfs()
        self.start_oozie()
        self.start_hbase()

        self.scr.writefl('Cluster started')


    def stop_cluster(self):
        """
        Stop the Hadoop services first, then stop the AWS cluster
        """
        self.stop_hbase()
        self.stop_oozie()
        self.stop_httpfs()
        self.stop_historyserver()
        self.stop_yarn()
        self.stop_dfs()
        self.stop_zookeeper()
        self.stop_cluster_aws()


    def stop_cluster_aws(self):
        """
        Stop the AWS cluster
        """
        ### TO DO: Need to wait until all instances are in a "stopped" state
        self.scr.writefl("Stopping cluster")
        ids = [instance.id for instance in self.inst_m + self.inst_s]
        self.conn.stop_instances(instance_ids=ids)
        key_file = self.KEY_FILE
        self.get_by_name(self.CLUSTER_NAME)
        self.KEY_FILE = key_file
        self.scr.writefl("  ok")

    
    def terminate_cluster(self):
        """
        Terminate all AWS cluster
        """
        self.scr.writefl("Terminating cluster")
        ids = [instance.id for instance in self.inst_m + self.inst_s]
        self.conn.terminate_instances(instance_ids=ids)
        self.clear()
        self.scr.writefl("  ok")

    
    def get_config(self):
        array = []
        array.append(("Cluster Name:", self.CLUSTER_NAME))
        array.append(("AMI:",self.IMAGE))
        array.append(("Security Group(s):", ', '.join(self.SECURITY_GROUPS)))
        array.append(("Master Instance Type:", self.MASTER_INSTANCE_TYPE))
        array.append(("Slave Instance Type:", self.SLAVE_INSTANCE_TYPE))
        array.append(("Number of Slave Nodes:", self.SLAVE_COUNT))
        array.append(("AWS SSH Key Name:", self.KEY_NAME))
        array.append(("AWS SSH Key Location:", self.KEY_FILE))
        return array
    
        #    def printscr(self, string):
        #        self.scr.addstr()

    def from_yaml(self, in_file_name):
        f = open(in_file_name, 'r')
        y = yaml.safe_load(f)
        f.close()

        self.clear()

        for item in y:
            key = item.keys()[0]
            value = item.values()[0]
            if key == "KEY_FILE":
                self.KEY_FILE = value
            elif key == "IMAGE":
                self.IMAGE = value
            elif key == "AVAIL_ZONE":
                self.AVAIL_ZONE = value
            elif key == "KEY_NAME":
                self.KEY_NAME = value
            elif key == "SECURITY_GROUPS":
                self.SECURITY_GROUPS = value
            elif key == "CLUSTER_NAME":
                self.CLUSTER_NAME = value
            elif key == "PROJECT_NAME":
                self.PROJECT_NAME = value
            elif key == "PROJECT_LEAD":
                self.PROJECT_LEAD = value
            elif key == "CHARGE_NR":
                self.CHARGE_NR = value
            elif key == "SLAVE_INSTANCE_TYPE":
                self.SLAVE_INSTANCE_TYPE = value
            elif key == "SLAVE_COUNT":
                self.SLAVE_COUNT = value
            elif key == "MASTER_INSTANCE_TYPE":
                self.MASTER_INSTANCE_TYPE = value
                # owner must be set by system
                #elif key == "OWNER":
                #    self.OWNER = value

        # create ec2 connection and set owner
        self.connect()

    def to_yaml(self, out_file_name):
        y = [
            {"KEY_FILE":              self.KEY_FILE},
            {"IMAGE":                 self.IMAGE},
            {"AVAIL_ZONE":            self.AVAIL_ZONE},
            {"KEY_NAME":              self.KEY_NAME},
            {"SECURITY_GROUPS":       self.SECURITY_GROUPS},
            {"CLUSTER_NAME":          self.CLUSTER_NAME},
            {"PROJECT_NAME":          self.PROJECT_NAME},
            {"PROJECT_LEAD":          self.PROJECT_LEAD},
            {"CHARGE_NR":             self.CHARGE_NR},
            {"SLAVE_INSTANCE_TYPE":   self.SLAVE_INSTANCE_TYPE},
            {"SLAVE_COUNT":           self.SLAVE_COUNT},
            {"MASTER_INSTANCE_TYPE":  self.MASTER_INSTANCE_TYPE},
            {"OWNER":                 self.OWNER}
        ]

        f = open(out_file_name, 'w')
        yaml.dump(y,f,default_flow_style=False)
        f.close()


    def get_by_name(self, cluster_name):
        """
        Query AWS EC2 instances looking for a cluster with a given
        "Name" tag. Fill in cluster object with retrieved values.
        Instances in a terminated state are excluded.

        :cluster_name: String value of the instance "Name" tag
        """

        self.clear()

        # get list of instances that match the Name tag
        instances = self.conn.get_only_instances(
            filters={"tag-key": "Name", "tag-value": cluster_name})
        # remove terminated instances from list
        instances = [instance for instance in instances
                     if not str(instance.state) == 'terminated']

        # make object lists of master and slaves
        try:
            self.inst_m = [instance for instance in instances
                           if re.match(r'master',instance.tags["Type"]) is not None]
        except Exception:
            self.inst_m = []
        try:
            self.inst_s = [instance for instance in instances
                           if re.match(r'slave',instance.tags["Type"]) is not None]
        except Exception:
            self.inst_s = []

        # if either of the lists is empty, do nothing
        if len(self.inst_m) == 0 or len(self.inst_s) == 0:
            return

        # assign values
        i_m = self.inst_m[0]
        i_s = self.inst_s[0]
        self.IMAGE                = str(i_m.image_id)
        self.AVAIL_ZONE           = str(i_m.placement)
        self.KEY_NAME             = str(i_m.key_name)
        self.SECURITY_GROUPS      = [str(gr.name) for gr in i_m.groups]
        self.CLUSTER_NAME         = str(cluster_name)
        self.PROJECT_NAME         = str(i_m.tags["Project"])
        self.PROJECT_LEAD         = str(i_m.tags["Project Lead"])
        self.CHARGE_NR            = str(i_m.tags["Charge Number"])
        self.SLAVE_INSTANCE_TYPE  = str(i_s.instance_type)
        self.SLAVE_COUNT          = len(self.inst_s)
        self.MASTER_INSTANCE_TYPE = str(i_m.instance_type)
        self.OWNER                = str(i_m.tags["Owner"])

        # set additional info
        self.save_instance_data()
