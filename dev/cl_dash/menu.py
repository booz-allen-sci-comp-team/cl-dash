import curses
from curses import panel
import boto
import boto.manage.cmdshell
import time
import os
import paramiko
import sys

#####################################################################
#          MENU MODULE
#####################################################################

# This is called by the class MyApp
# It makes a Menu by taking in a MyApp object and enumerating the list
# Automatically adds exit to each menu
class Menu(object):

    def __init__(self, items, stdscreen, name):
        self.name = name    
        self.window = curses.newwin(20,60,3,0)
        self.window.keypad(1) # enables arrow keys
        self.panel = panel.new_panel(self.window) # makes a panel, adds it to the window
        #self.panel.hide() # don't show it just yet
        
        self.window2 = curses.newwin(20,60,3,61) # sets window to stdscreen
        self.window2.keypad(1) # enables arrow keys
        self.panel2 = panel.new_panel(self.window2) # makes a panel, adds it to the window
        self.window2.addstr(15,1, "%s"%self.name, curses.color_pair(1))
        #self.window2.immedok(1)
        #self.panel2.hide() # don't show it just yet
        
        panel.update_panels()
        curses.doupdate()
        
        self.position = 0 # cursor position starts at first list item
        self.items = items
        self.items.append(('Exit','Exit')) # Takes the list for the menu and appends exit

    # called by arrow keys
    def navigate(self, n):
        self.position += n
        # prevents scrolling too far up or down
        if self.position < 0:
            self.position = 0
        elif self.position >= len(self.items):
            self.position = len(self.items)-1

    # display the selected menu object
    def display(self):
        self.panel.top() # bring to top of stack
        self.panel.show() # make visible
        #below = self.panel.below()
        #print below
        
        #self.window.clear() # clear text
        
        self.panel2.top() # bring to top of stack
        self.panel2.show() # make visible
        #self.panel2.below().hide()
        #self.window2.clear() # clear text
        
        #self.window.border('|', '|', '-', '-', '+', '+', '+', '+')
        #self.window2.border('|', '|', '-', '-', '+', '+', '+', '+')
        panel.update_panels()
        curses.doupdate()
        
        # keeps running until an item is selected
        while True:
            #self.window.refresh()
            #self.window2.refresh()
            #curses.doupdate()
            # This for loop creates the menu
            for index, item in enumerate(self.items):
                if index == self.position:
                    mode = curses.A_REVERSE # reverse mode highlights the text
                else:
                    mode = curses.A_NORMAL
                # creates the display line, then displays it
                msg = '%d. %s' % (index+1, item[0]) # index+1 refers to the item number being displayed
                self.window.addstr(1+index, 5, msg, mode) # index+1 refers to the line offset
            # Waits for key input - only accepts up/down/enter
            key = self.window.getch()

            # on key press
            if key in [curses.KEY_ENTER, ord('\n')]:
                # exits if on last item
                if self.position == len(self.items)-1:
                    break
                # runs associated command for line
                else:
                    self.items[self.position][1]()
#                     if self.items[self.position][2] == False:
#                         self.items[self.position][1]()
#                     else:
#                         self.items[self.position][1](eval(self.items[self.position][2])) # calling the associated method of the item from the dictionary
            # moves up/down
            elif key == curses.KEY_UP:
                self.navigate(-1)
            elif key == curses.KEY_DOWN:
                self.navigate(1)
            # handle all number inputs
            elif (key-48) == len(self.items):
                break # handle exit
            elif key >= 49 and key <= 57:
                if (key-48)<len(self.items):
                    self.items[key-49][1]()
        # when user selects exit, breaks and comes here
        self.window.clear()
        self.window2.clear()
        self.panel.hide()
        self.panel2.hide()
        panel.update_panels()
        curses.doupdate()
        
