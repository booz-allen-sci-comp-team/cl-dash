#!/bin/bash

OUT=output-standalone/data.assoc

echo "Compiling results from temporary directory:"

echo "echo \" CHR                                  SNP         BP   A1      F_A      F_U   A2        CHISQ            P           OR \" > $OUT"
echo " CHR                                  SNP         BP   A1      F_A      F_U   A2        CHISQ            P           OR " > $OUT

echo "find output-standalone/tmp -name \"part-*.assoc\" \
    | sort \
    | xargs -I % cat % \
    | grep -v \"^ *CHR  *SNP  *BP  *A1  *F_A\" >> $OUT"
find output-standalone/tmp -name "part-*.assoc" \
    | sort \
    | xargs -I % cat % \
    | grep -v "^ *CHR  *SNP  *BP  *A1  *F_A" >> $OUT

echo "  Done."
