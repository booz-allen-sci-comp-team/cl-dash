#!/bin/bash

echo "Preparing input in HDFS:"

echo "rm -r output-streaming"
rm -r output-streaming

echo "mkdir output-streaming"
mkdir output-streaming

echo "hadoop fs -rm -r in out"
hadoop fs -rm -r in out

echo "hadoop fs -mkdir in"
hadoop fs -mkdir in

echo "hadoop fs -put data/tped/* in/"
hadoop fs -put data/tped/* in/

echo "  Done."
