#!/bin/bash

OUT=output-streaming/data.assoc

echo "Retrieving results from HDFS:"

echo "echo \" CHR                                  SNP         BP   A1      F_A      F_U   A2        CHISQ            P           OR \" > $OUT"
echo " CHR                                  SNP         BP   A1      F_A      F_U   A2        CHISQ            P           OR " > $OUT

echo "hadoop fs -cat out/part-* >> $OUT"
hadoop fs -cat out/part-* >> $OUT

echo "  Done."
