#!/usr/bin/perl -w
use warnings;

# output log and data table filename prefix
my $out_prefix = "plink-out";

# filename of locally staged TPED from input stream
my $tmp_tped = "plink-tmp.tped";

# open temporary tped destination
open(TPED, "> $tmp_tped") || die "died at TPED open $!\n";

# dump input stream to temp tped file
while (<STDIN>) {
	print TPED;	
}
close(TPED) || die "died at TPED close $!\n";

# assumption: tfam exists/is staged already in distributed cache
my $dist_tfam = "gsm.tfam";

# assumption: plink executable is installed system wide
# call plink
system("/usr/lib/plink/plink --noweb --tped $tmp_tped --tfam tfam.txt --assoc --out $out_prefix --silent --maf 0.01 --pfilter 0.1");
#system("/usr/lib/plink/plink --noweb --tped $tmp_tped --tfam gsm.tfam --assoc --out $out_prefix --maf 0.01");

# emit association study results (text table) to output stream without header
if (-f "$out_prefix.assoc") {
    open(ASSOC, "< $out_prefix.assoc") || die "died at ASSOC open $!\n";
    my $line = <ASSOC>;
    while ($line = <ASSOC>) {
	my @words = split /\s+/,$line;
	my $key = sprintf "%4d %11d", $words[1], $words[3];
	print "$key\t$line";
    }
    close(ASSOC) || die "died at ASSOC close $!\n";
}
# Uncomment below if troubleshooting-- the content of PLINK's log 
# will be sent to output if not results are generated from association test.
#else {	system("cat $out_prefix.log"); }
