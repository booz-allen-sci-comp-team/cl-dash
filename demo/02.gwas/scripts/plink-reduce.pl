#!/usr/bin/perl -w
use warnings;

# Discard key and output value
while (my $line = <STDIN>) {
    my ($key,$value) = split /\t/, $line;
    print $value;
}
