#!/bin/bash

echo "Running MapReduce:"
hadoop jar /usr/local/hadoop/share/hadoop/tools/lib/hadoop-streaming-2.2.0.jar \
    -files ./scripts/plink-map.pl,./scripts/plink-reduce.pl,./data/tfam.txt \
    -input hdfs://master:9000/user/hduser/in \
    -output out \
    -mapper plink-map.pl \
    -reducer plink-reduce.pl
echo "  Done."
