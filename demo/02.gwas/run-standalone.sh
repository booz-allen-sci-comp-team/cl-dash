#!/bin/bash

find data/tped -name "part-*" \
    | sed "s/.*\///" \
    | sort \
    | xargs -I % \
    plink \
    --noweb \
    --tped ./data/tped/% \
    --tfam ./data/tfam.txt \
    --assoc \
    --out output-standalone/tmp/% \
    --silent \
    --maf 0.01 \
    --pfilter 0.1
