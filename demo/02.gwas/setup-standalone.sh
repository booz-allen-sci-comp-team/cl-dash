#!/bin/bash

echo "Preparing output directory:"

echo "rm -r output-standalone"
rm -r output-standalone

echo "mkdir -p output-standalone/tmp"
mkdir -p output-standalone/tmp

echo "  Done."
