#!/bin/bash

OUT="output-standalone.txt"
echo -n "Begin: " > $OUT
date >> $OUT
gunzip -c data/* | scripts/cnt-aa-map-1.pl >> output-standalone.txt
echo -n "End: " >> $OUT
date >> $OUT
