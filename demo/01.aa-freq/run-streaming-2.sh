#!/bin/bash

OUT="output-streaming-2.txt"

echo "Preparing input in HDFS:" > $OUT
hadoop fs -rm -r in out
hadoop fs -mkdir in
hadoop fs -put data/*.gz in/
echo "  Done." >> $OUT

echo "Running MapReduce:" >> $OUT
echo -n "Begin: " >> $OUT
date >> $OUT
hadoop jar /usr/local/hadoop/share/hadoop/tools/lib/hadoop-streaming-2.2.0.jar \
    -files ./scripts/cnt-aa-map-2.pl,./scripts/cnt-aa-reduce.pl \
    -input hdfs://master:9000/user/hduser/in \
    -output out \
    -mapper cnt-aa-map-2.pl \
    -reducer cnt-aa-reduce.pl
echo -n "End: " >> $OUT
date >> $OUT
echo "  Done." >> $OUT

echo "Displaying output:" >> $OUT
hadoop fs -cat out/part-00000 >> $OUT
echo "  Done". >> $OUT
