#!/usr/bin/perl -w

use strict;
use Bio::SeqIO;
use IO::Uncompress::Gunzip;
use Data::Dumper;

my %aa_cnt;

my $gz = new IO::Uncompress::Gunzip \*STDIN;
my $in = Bio::SeqIO->new(-fh => $gz, -format => "fasta");
#my $in = Bio::SeqIO->new(-fh => \*STDIN, -format => "fasta");

while (my $seq = $in->next_seq) {
    for my $aa (split //, $seq->seq) {
	if (exists $aa_cnt{$aa}) {
	    $aa_cnt{$aa} += 1;
	}
	else {
	    $aa_cnt{$aa} = 1;
	}
    }
}

for my $key (sort keys %aa_cnt) {
    print "$key\t$aa_cnt{$key}\n";
}
