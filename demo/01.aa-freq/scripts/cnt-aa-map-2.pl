#!/usr/bin/perl -w

use strict;
use Bio::SeqIO;
use IO::Uncompress::Gunzip;
use Data::Dumper;

my %aa_cnt;

my $gz = new IO::Uncompress::Gunzip \*STDIN;
my $in = Bio::SeqIO->new(-fh => $gz, -format => "fasta");
#my $in = Bio::SeqIO->new(-fh => \*STDIN, -format => "fasta");

while (my $seq = $in->next_seq) {
    for my $aa (split //, $seq->seq) {
	print "$aa\t1\n";
    }
}
