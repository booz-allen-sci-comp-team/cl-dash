#!/usr/bin/perl -w

use strict;
use Data::Dumper;

my $key_old = ""; # The MapReduce key is an amino acid symbol
my $cnt; # The value is a count

while (my $line = <STDIN>) {
    chomp $line;
    my ($key_new, $val) = split "\t", $line;

    # new key found
    if ($key_new ne $key_old) {
	if ($key_old ne "") {
	    print "$key_old\t$cnt\n";
	}
	$cnt = $val;
	$key_old = $key_new;
    }
    # same key
    else {
	$cnt += $val;
    }
}

# print last key
if ($key_old ne "") {
    print "$key_old\t$cnt\n";
}
